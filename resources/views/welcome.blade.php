<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TRANS TV | KARYAWAN</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/icon/css/font-awesome.min.css">

    </head>
    <body class="antialiased">
        <div class="topnav">
            <a class="active" href="#home">Daftar Karyawan</a>
          </div>   
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>    
          
          <hr width="75%    ">

            <a href="#">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalImport">
                    <i class="fa fa-plus position-left"></i> Tambah Data Karyawan
                </button>
            </a>
            <a href="#">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalImportCuti">
                    <i class="fa fa-plus position-left"></i> Tambah Data Cuti Karyawan 
                </button>
            </a>
            @php
             $no = 1;   
            @endphp

            <table width="100%">
                <tr>
                    <td  width="10%"> No.</td>
                    <td  width="50%"> Nama</td>
                    <td  width="40%"> Action</td>
                </tr>
                <tr>
                    
                    <td>
                        {{$no}}
                    </td>
                    <td> Nama</td>
                    <td>
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-info position-left"></i> Detail
                        </button>
                        <button type="button" class="btn btn-warning">
                            <i class="fa fa-plus position-left"></i> Edit
                        </button>
                        <button type="button" class="btn btn-success">
                            <i class="fa fa-print position-left"></i> Cetak
                        </button>
                        <button type="button" class="btn btn-danger">
                            <i class="fa fa-trash position-left"></i> Hapus
                        </button>
                    </td>


                </tr>
            </table>
            <div class="modal fade" id="ModalImport" tabindex="-1" role="dialog" aria-labelledby="ModalImport1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalImport1">Upload Excel File</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/karyawan/import" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="log_absent_id" value="#" />
                                    <input type="file" name="file" required="required">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Import</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal fade" id="ModalImportCuti" tabindex="-1" role="dialog" aria-labelledby="ModalImport1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalImport1">Upload Excel Cuti File</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/karyawan/import/cuti" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="log_absent_id" value="#" />
                                    <input type="file" name="file" required="required">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Import</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
