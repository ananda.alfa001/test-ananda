<?php

namespace App\Imports;

use App\Models\karyawan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class karyawanImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new karyawan([
                'npk' => $row['npk'],
                'nama' => $row['nama'],
                'jenis_kelamin' => $row['jenis_kelamin'],
                'tanggal_lahir' => date('Y-m-d', strtotime($row['tanggal_lahir'])) ,
        ]);
    }
}
