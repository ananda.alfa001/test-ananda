<?php

namespace App\Imports;

use App\saldocuti;
use Maatwebsite\Excel\Concerns\ToModel;

class SaldoImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new saldocuti([
            //
        ]);
    }
}
