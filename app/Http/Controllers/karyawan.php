<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\karyawanImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class karyawan extends Controller
{
    public function index()
    {
        //SHOW KARYAWAN
    }
    public function create()
    {
        //CREATE KARYAWAN
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //EDIT KARYAWAN
    }

    public function update(Request $request, $id)
    {
        //UPDATE KARYAWAN
    }

    public function destroy($id)
    {
        //HAPUS KARYAWAN
    }

    public function karyawanImport(Request $request) 
	{
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('karyawan',$nama_file);
		Excel::import(new karyawanImport, public_path('/karyawan/'.$nama_file));
		return redirect('/');
	}
	public function karyawanImportCuti(Request $request) 
	{
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('cuti',$nama_file);
		Excel::import(new karyawanImport, public_path('/cuti/'.$nama_file));
		return redirect('/');
	}
}
